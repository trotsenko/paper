# The B-type lamin is required for somatic repression of testis-specific gene clusters #
## Y. Y. Shevelyova, S. A. Lavrova, L. M. Mikhaylovab, I. D. Nurminskyc, R. J. Kulathinald, K. S. Egorovaa, Y. M. Rozovskya,and D. I. Nurminskyb ##
http://www.pnas.org/cgi/doi/10.1073/pnas.0811933106


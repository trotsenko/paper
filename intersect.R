library(GenomicRanges) 
library(rtracklayer)
inter <- function(col, row) {
    q <- findOverlaps(row, col, ignore.strand = T )
    col <- col[subjectHits(q)]
    row <- row[queryHits(q)]
    rez <- granges(pintersect(col, row, ignore.strand = T))
    rez$col <- factor(score(col))
    rez$row <- factor(score(row))
    sapply(split(rez, rez$col),
           function(x) sapply(split(width(x), x$row), sum))
}

fil <- read.delim("model/fillion.bed.gz", header = F)
fil <- with(fil, GRanges(V1, IRanges(V2, V3, names = V4), score = V5))
hm <- import("model/hmm4-08a.bed.gz")
seqlevels(hm, T) <- "chrX"
mc.s2 <- import("model/s2.9state.model.wig.gz")
x <- inter(hm, mc.s2)
tb <- lapply(list(count = x,
     row = round(x / rowSums(x) * 100, 2),
     col  = t(round(t(x) / colSums(x) * 100, 2))
     ), as.data.frame)
library(WriteXLS)
WriteXLS("tb", "s2.9stStatChrX.xls",row.names = T)

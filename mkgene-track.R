library("rtracklayer");
library("GenomicRanges");
x <- import("Drosophila_melanogaster.BDGP5.78.gtf.gz");
x <- x[ x$type == "gene"];
seqlevels(x, TRUE) <- "2R";
seqlevels(x) <- "chr2R";
x$score <- 0;
names(x) <- x$gene_name;
gene.type <- split(x, x$gene_biotype);
for ( nm in names(gene.type)) {
    export( gene.type[[nm]], paste0("rez/2R-", nm, ".bed.gz"));
}
